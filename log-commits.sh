#!/bin/sh

echo "Running build..."
echo "Graal current commit: $GRAAL_CURRENT_COMMIT"
echo "Graal last commit: $GRAAL_LAST_COMMIT"
echo "Micronaut current commit: $MN_CURRENT_COMMIT"
echo "Micronaut last commit: $MN_LAST_COMMIT"
